package CalculatingPi;

public abstract class CalculatingThread<T> {
	protected long fromIndex;
	protected long toIndex;
	protected boolean running;
	

	public abstract void shutdown();

	public abstract T getResult();
}
