package CalculatingPi;

import static java.math.BigDecimal.ROUND_HALF_UP;

import java.math.BigDecimal;

public class BigCalculatingThread extends CalculatingThread<BigDecimal> implements Runnable {

	int input;

	public BigCalculatingThread(int input) {
		this.input = input;
	}

	private BigDecimal value;

	@Override
	public void shutdown() {
	}

	@Override
	public BigDecimal getResult() {

		return value;
	}

	@Override
	public void run() {

		BigDecimal resutl = new BigDecimal(0);
		BigDecimal ONE = new BigDecimal(1);
		BigDecimal TOW = new BigDecimal(2);

		for (long i = 1; i < toIndex; i += 2) {

			System.out.println("Enter '1' to stop...");
			if (running == true) {
				BigDecimal a = new BigDecimal(i);
				// resutl= resutl + ((1.0 / (2.0 * i - 1)) - (1.0 / (2.0 * i +
				// 1)));
				resutl = resutl.add((ONE.divide(TOW.multiply(a).subtract(ONE), input, ROUND_HALF_UP))
						.subtract((ONE.divide(TOW.multiply(a).add(ONE), input, ROUND_HALF_UP))));
			} else {
				break;
			}
		}
		value = resutl.multiply(new BigDecimal(4));// =resutl*4
		running = false;

	}

}
