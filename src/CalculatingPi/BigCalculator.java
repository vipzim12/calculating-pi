package CalculatingPi;

import java.math.BigDecimal;
import java.util.Scanner;

public class BigCalculator implements Calculator {

	BigCalculatingThread bigCalculatingThread;

	@Override
	public void calculate(int decimalDegree) {
		
		Scanner in = new Scanner(System.in);
		bigCalculatingThread = new BigCalculatingThread(decimalDegree);
		Thread t1 = new Thread(bigCalculatingThread);
		bigCalculatingThread.toIndex = (long) ((Math.pow(10, decimalDegree + 1) - 1) / 2);
		bigCalculatingThread.running = true;
		t1.start();
		System.out.println("Enter '1' to stop...");
		while (bigCalculatingThread.running == true) {
			int input = in.nextInt();
			if (input == 1) {
				bigCalculatingThread.running = false;
			}
		}
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		printResult(decimalDegree);

	}

	@Override
	public void printResult(int decimalDegree) {
		System.out.println("BigCalculator");
		System.out.println("Pi= " + bigCalculatingThread.getResult());
	}

	@Override
	public void terminate() {

	}

}
