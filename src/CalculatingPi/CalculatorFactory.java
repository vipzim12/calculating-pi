package CalculatingPi;

public class CalculatorFactory {
	public Calculator getCalculatorFactory(int decimalDegree) {
		if (decimalDegree >= 18) {
			return new BigCalculator();
		} else
			return new SmallCalculator();

	}
}
