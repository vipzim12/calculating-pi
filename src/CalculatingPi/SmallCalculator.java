package CalculatingPi;

import java.util.Scanner;

public class SmallCalculator implements Calculator {

	Scanner in = new Scanner(System.in);
	SmallCalculatingThread smallCalculatingThread = new SmallCalculatingThread();
	Thread t1 = new Thread(smallCalculatingThread);

	@Override
	public void calculate(int decimalDegree) {

		smallCalculatingThread.toIndex = (long) ((Math.pow(10, decimalDegree + 1) - 1) / 2);
		smallCalculatingThread.running = true;
		t1.start();
		System.out.println("Enter '1' to stop...");
		while (smallCalculatingThread.running == true) {
			int input = in.nextInt();
			if (input == 1) {
				smallCalculatingThread.running = false;
			}

		}
		
		printResult(decimalDegree);
	}

	@Override
	public void printResult(int decimalDegree) {
		double a = Math.pow(10, decimalDegree);
		System.out.println("SmallCalculator");
		System.out.println("Pi= " + Math.round(smallCalculatingThread.getResult() * a) / a);
	}

	@Override
	public void terminate() {

	}

}
