package CalculatingPi;

import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyException;

import javax.xml.crypto.dsig.keyinfo.KeyValue;

public class Main {
	public static void main(String[] args) throws Exception, IOException, KeyException {
		CalculatorFactory calculatorFactory = new CalculatorFactory();
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Nhap so chu so thap phan muon lay: ");
		int input = Integer.parseInt(in.readLine());
		Calculator calculator = calculatorFactory.getCalculatorFactory(input);
		calculator.calculate(input);

	}

}
