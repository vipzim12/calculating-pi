package CalculatingPi;

public class SmallCalculatingThread extends CalculatingThread<Double> implements Runnable {

	private double value;

	@Override
	public void shutdown() {

	}

	@Override
	public Double getResult() {
		return value;
	}

	@Override
	public void run() {
		double m = 0;
		for (long i = 1; i < toIndex; i += 2) {
			System.out.println("Enter '1' to stop...");
			if (running == true) {
				m = m + ((1.0 / (2.0 * i - 1)) - (1.0 / (2.0 * i + 1)));
			} else {

				break;
			}
		}
		value = m * 4;
		running = false;
	}

}
