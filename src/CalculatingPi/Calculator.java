package CalculatingPi;

public interface Calculator {
	public void calculate(int decimal);

	public void terminate();

	public void printResult(int decimalDegree);
	
	
}
